import sys
import gi
import math
from pysine import sine

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk

def freq(note,octave):
    return (octave*65.41)*math.pow(2,note/12)

def play(f):
    sine(frequency=f, duration=1.0) 
    return 
octave = 4
tone_dict = {"C":0,"C#":1,"D":2,"D#":3,"E":4,"F":5,"F#":6,"G":7,"G#":8,"A":9,"A#":10,"B":11}
#global octave=4

def make_buttons(fixed):
    theta = 2*math.pi / len(tone_dict)
    center = 100
    radius = 100
    global octave
    for i,(k,v) in enumerate(tone_dict.items()):
        angle = i * theta
        dx = int(radius * math.cos(angle))
        dy = int(radius * math.sin(angle))
        button = Gtk.Button.new_with_label(k)
        button.connect("clicked", lambda i:play(freq(v,octave)))
        fixed.put(button,center+dx,center+dy)

class ButtonWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Tone Clock")
        self.set_border_width(10)

        hbox = Gtk.Box(spacing=6)
        fixed = Gtk.Fixed()
        #self.add(hbox)
        self.add(fixed)
        GLib.set_application_name('Tone Clock')
        make_buttons(fixed=fixed)
        self.connect("key_press_event",self.key_press)
    def key_press(self, widget, event):
        # TODO find a non deprecated way of doing this lol
        global octave
        if event.keyval == 65362:
            octave +=1
        if event.keyval == 65364:
            octave -=1
        s = event.string
        if s.isnumeric():
            play(freq(int(s),octave))
        elif s.lower() == "t":
            play(freq(10,octave))
        elif s.lower() == "e":
            play(freq(11,octave))
        

win = ButtonWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()

